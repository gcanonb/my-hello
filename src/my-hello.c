#include <config.h>
#include <stdio.h>

int main (void)
{
  //puts("Hello World!");
  //the macro PACKAGE_STRING defined in config.h
  //if macro in string, should be around extra double quota!
  puts("Hello World! This is "PACKAGE_STRING" for xbps-src testing."); 
  return 0;
}
